/*
** gere_keys.c for wolf3D in /home/blackbird/work/wolf3D/gere_keys
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Jan 12 11:22:38 2014 romaric
** Last update Sun Jan 12 11:44:27 2014 romaric
*/

#include "../headers/wolf.h"

int     gere_key(int keycode, t_mlx *m)
{
  if (keycode == 65307)
    exit (EXIT_SUCCESS);
  else if (keycode == 65363 || keycode == 100)
    m->ang = m->ang - 7;
  else if (keycode == 65361 || keycode == 97)
    m->ang = m->ang + 7;
  else if (keycode == 65362 || keycode == 119)
    go(m);
  else if (keycode == 65364 || keycode == 115)
    back(m);
  calc(m, m->data);
  mlx_put_image_to_window(m->mlx_ptr, m->win_ptr, m->img, 0, 0);
  return (0);
}
