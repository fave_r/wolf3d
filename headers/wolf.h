/*
** wolf.h for wolf3D in /home/blackbird/work/wolf3D
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Fri Jan 10 16:09:55 2014 romaric
** Last update Mon Jan 13 09:33:49 2014 romaric
*/

#ifndef __wolf3D__
#define __wolf3D__

#include <mlx.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include "fmod.h"
#include "X.h"


typedef struct  s_mlx
{
  void          *mlx_ptr;
  void          *win_ptr;
  float		x;
  float		y;
  int		ang;
  char		*data;
  void		*img;
}                t_mlx;

int	gere_key(int keycode, t_mlx *m);
int	gere_expose(t_mlx *mlx);
void	mlx_put_pixel_to_image(char *data, int x, int y, int color);
void	backgroudO(char *data);
int	main(int ac, char **av);
void	backgroudB(char *data);
void	draw_wall(float wall, char *data, int x);
void	calc(t_mlx *m, char *data);
void	go(t_mlx *m);
void    back(t_mlx *m);
int     my_strlen(char *str);
int     my_putstr(char *str, int op);
void	fmode(char *str);

#endif
