/*
** back.c for wolf3D in /home/blackbird/work/wolf3D/walk
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Jan 12 11:12:01 2014 romaric
** Last update Tue Jan 14 13:46:24 2014 romaric
*/

#include "../headers/wolf.h"

int     mape[10][10] =
  {
    {1, 3, 1, 1, 1, 1, 1, 1, 1, 1},
    {1, 0, 1, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
    {1, 0, 1, 2, 1, 1, 1, 0, 0, 1},
    {1, 0, 1, 1, 1, 0, 1, 0, 0, 1},
    {1, 0, 1, 0, 1, 0, 1, 0, 0, 1},
    {1, 0, 1, 0, 0, 0, 1, 0, 0, 1},
    {1, 0, 1, 0, 1, 1, 1, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
  };

void    back(t_mlx *m)
{
  float angle;

  angle = m->ang * (M_PI / 180);
  if (mape[(int)(m->x - (cos(angle)/10))][(int)(m->y - (sin(angle)/10))] == 0)
    {
      m->x -= cos(angle) / 10;
      m->y -= sin(angle) / 10;
    }
  else if (mape[(int)(m->x - (cos(angle)/10))]
           [(int)(m->y - (sin(angle)/10))] == 2)
    {
      my_putstr("\033[31mCONGRATULATION U VE FINISHED THE MAP!\033[0;m", 1);
      write(1, "\n", 1);
      exit(EXIT_SUCCESS);
    }
  else if (mape[(int)(m->x - (cos(angle)/10))]
           [(int)(m->y - (sin(angle)/10))] == 3)
    {
      my_putstr("\033[31mU RE A FUCKING COWARD!\033[0;m", 1);
      write(1, "\n", 1);
      exit(EXIT_SUCCESS);
    }
}
