##
## Makefile for wolf3D in /home/blackbird/work/wolf3D
##
## Made by romaric
## Login   <fave_r@epitech.net>
##
## Started on  Fri Jan 10 16:25:31 2014 romaric
## Last update Thu Mar  6 21:55:10 2014 romaric
##

CC=     gcc

RM=     rm -f

CFLAGS= -Wextra -Wall -Werror -ggdb3 -g3 -I./minilibx/

NAME=   wolf3d

SRCS=	main.c \
	./compute/calc.c \
	./draw/draw_walls.c \
	./walk/go.c \
	./walk/back.c \
	./utils/char.c \
	./gere_keys/gere_keys.c \
	./expose/expose.c \
	./utils/mlx_pixel.c \
	fmod.c

MINLIB=	-L./minilibx/ -lmlx -L/usr/lib64/X11 -lXext -lX11 -lm ./fmodapi44431linux/api/lib/libfmodex64.so

OBJS=   $(SRCS:.c=.o)

all:    $(NAME)

$(NAME):        $(OBJS)
		$(CC) $(OBJS) -o $(NAME) $(LDFLAGS) $(MINLIB)

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re:	fclean all

.PHONY: all fclean re