/*
** char.c for wolf3D in /home/blackbird/work/wolf3D
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sat Jan 11 19:28:33 2014 romaric
** Last update Sun Jan 12 11:44:55 2014 romaric
*/

#include "../headers/wolf.h"

int     my_strlen(char *str)
{
  int   x;

  x = 0;
  while (str[x] != '\0' && str[x] != '\n')
    x++;
  return (x);
}

int	my_putstr(char *str, int op)
{
  return (write(op, str, my_strlen(str)));
}
