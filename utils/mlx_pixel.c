/*
** mlx_pixel.c for wolf3D in /home/blackbird/work/wolf3D/utils
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Jan 12 11:29:16 2014 romaric
** Last update Sat Feb 22 13:10:43 2014 romaric
*/

#include "../headers/wolf.h"

void    mlx_put_pixel_to_image(char *data, int x, int y, int color)
{
  char  *ptr;
  char *color_ptr;

  ptr = data + y * 2400 + x * 4;
  color_ptr = (char*)&color;
  ptr[0] = color_ptr[0];
  ptr[1] = color_ptr[1];
  ptr[2] = color_ptr[2];
}
