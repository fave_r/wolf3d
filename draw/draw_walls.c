/*
** draw_walls.c for wolf3D in /home/blackbird/work/wolf3D/draw
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Jan 12 11:03:06 2014 romaric
** Last update Sun Jan 12 11:43:25 2014 romaric
*/

#include "../headers/wolf.h"

void    draw_wall(float wall, char *data, int x)
{
  int   y;

  y = 0;
  while (y < 600)
    {
      if (y < ((600 - wall * 2) / 2))
        mlx_put_pixel_to_image(data, x, y, 0xB0E0E6);
      else if (y < (600 - (600 - wall * 2) / 2))
        mlx_put_pixel_to_image(data, x, y, 0x778899);
      else
        mlx_put_pixel_to_image(data, x, y, 0x9ACD32);
      y++;
    }
}
