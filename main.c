/*
** main.c for wolf3D in /home/blackbird/work/wolf3D
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Fri Jan 10 16:07:04 2014 romaric
** Last update Thu Mar  6 21:53:19 2014 romaric
*/

#include "./headers/wolf.h"

int     main(__attribute__((unused))int ac, char **av)
{
  t_mlx mlx;
  int	bpp;
  int	sizeline;
  int	endian;

  mlx.mlx_ptr = mlx_init();
  if (mlx.mlx_ptr == NULL)
    return (0);
  fmode(av[1]);
  mlx.x = 1.5;
  mlx.y = 1.5;
  mlx.ang = 0;
  mlx.win_ptr = mlx_new_window(mlx.mlx_ptr, 600, 600, "Wolf3D");
  mlx_key_hook(mlx.win_ptr, &gere_key, &mlx);
  mlx_hook(mlx.win_ptr, 2, KeyPressMask, gere_key, &mlx);
  mlx.img = mlx_new_image(mlx.mlx_ptr, 600, 600);
  mlx.data = mlx_get_data_addr(mlx.img, &bpp, &sizeline, &endian);
  calc(&mlx, mlx.data);
  mlx_put_image_to_window(mlx.mlx_ptr, mlx.win_ptr, mlx.img, 0, 0);
  mlx_expose_hook(mlx.win_ptr, &gere_expose, &mlx);
  mlx_loop(mlx.mlx_ptr);
  return (0);
}
