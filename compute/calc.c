/*
** calc.c for wolf3D in /home/blackbird/work/wolf3D/compute
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Jan 12 10:48:23 2014 romaric
** Last update Sun Jan 12 11:42:53 2014 romaric
*/

#include "../headers/wolf.h"

int     maps[10][10] =
  {
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    {1, 0, 1, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
    {1, 0, 1, 2, 1, 1, 1, 0, 0, 1},
    {1, 0, 1, 1, 1, 0, 1, 0, 0, 1},
    {1, 0, 1, 0, 1, 0, 1, 0, 0, 1},
    {1, 0, 1, 0, 0, 0, 1, 0, 0, 1},
    {1, 0, 1, 0, 1, 1, 1, 0, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
  };

void    calc(t_mlx *m, char *data)
{
  int i;
  float       x;
  float       angle;
  float       p;
  float       x1;
  float       y1;
  float       wall;

  angle = m->ang * (M_PI / 180);
  i = 0;
  while (i < 600)
    {
      x = 0.5;
      p = (600.0 * 0.5 - i) / 600.0;
      x1 = x * cos(angle) - p * sin(angle);
      y1 = x * sin(angle) + p * cos(angle);
      while (maps[(int)(m->x + x * x1)][(int)(m->y + x * y1)] == 0)
	x += 0.01;
      wall = 600.0 / (2 * x);
      draw_wall(wall, data, i);
      i++;
    }
}
