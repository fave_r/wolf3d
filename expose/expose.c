/*
** expose.c for wolf3D in /home/blackbird/work/wolf3D/expose
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Jan 12 11:26:06 2014 romaric
** Last update Sun Jan 12 11:43:55 2014 romaric
*/

#include "../headers/wolf.h"

int     gere_expose(t_mlx *m)
{
  mlx_put_image_to_window(m->mlx_ptr, m->win_ptr, m->img, 0, 0);
  return (0);
}
