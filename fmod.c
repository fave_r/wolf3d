/*
** fmod.c for wolf3D in /home/blackbird/work/wolf3D
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Jan 12 13:31:04 2014 romaric
** Last update Thu Jan 16 14:57:53 2014 romaric
*/

#include "./headers/wolf.h"

void	fmode(char *str)
{
  FMOD_SYSTEM *system;
  FMOD_SOUND *musique;
  FMOD_CHANNEL *channel;
  FMOD_RESULT resultat;

  if (str == NULL)
    str = "battle.mp3";
  FMOD_System_Create(&system);
  FMOD_System_Init(system, 2, FMOD_INIT_NORMAL, NULL);
  FMOD_Sound_SetLoopCount(musique, -1);
  resultat = FMOD_System_CreateSound(system, str, FMOD_SOFTWARE
				     | FMOD_2D | FMOD_CREATESTREAM, 0, &musique);
  if (resultat != FMOD_OK)
    {
      my_putstr("Cannot find ", 2);
      my_putstr(str, 2);
      my_putstr(", put this file next to the executable 'wolf3d'", 2);
      write(2, "\n", 1);
      FMOD_System_CreateSound(system, "battle.mp3", FMOD_SOFTWARE
			      | FMOD_2D | FMOD_CREATESTREAM, 0, &musique);
    }
  FMOD_System_GetChannel(system, 9, &channel);
  FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, musique, 0, NULL);
}
